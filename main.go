package main

import (
	"fmt"
)

func main() {
	client := NewClient()

	title, err := client.getTitle()
	if err != nil {
		panic("HELP")
	}
	fmt.Println(title)
}
