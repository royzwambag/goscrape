package main

import (
	"errors"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

// Client is an http client that contains an url
type Client struct {
	http *http.Client
	url  string
}

// NewClient creates a Client with sensible defaults
func NewClient() Client {
	client := http.Client{
		Timeout: time.Second * 30,
	}
	return Client{
		http: &client,
		url:  "https://royzwambag.nl/",
	}
}

func (client *Client) get() (*http.Response, error) {
	request, err := http.NewRequest("GET", client.url, nil)
	if err != nil {
		return nil, err
	}

	request.Header.Set("User-Agent", "goscrape")

	response, err := client.http.Do(request)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (client *Client) bodyAsString() (string, error) {
	response, err := client.get()
	defer response.Body.Close()
	if err != nil {
		return "", err
	}

	body, err := ioutil.ReadAll(response.Body)
	return string(body), err
}

func (client *Client) getTitle() (string, error) {
	body, err := client.bodyAsString()
	if err != nil {
		return "", err
	}

	titleStartIndex := strings.Index(body, "<title>")
	if titleStartIndex == -1 {
		return "", errors.New("Title opening tag not found")
	}
	// We add 7, so we do not include the <title> tag
	titleStartIndex += 7

	titleEndIndex := strings.Index(body, "</title>")
	if titleEndIndex == -1 {
		return "", errors.New("Title closing tag not found")
	}

	return body[titleStartIndex:titleEndIndex], nil
}
